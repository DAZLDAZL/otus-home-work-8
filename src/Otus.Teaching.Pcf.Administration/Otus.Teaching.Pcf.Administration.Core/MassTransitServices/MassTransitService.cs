﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.MassTransitServices
{
    public class MassTransitService : IHostedService
    {
        private IBusControl _busControl;
        private int executionCount = 0;
        private readonly ILogger<MassTransitService> _logger;
        private Timer? _timer = null;

        public MassTransitService(IBusControl busControl, ILogger<MassTransitService> logger, IConfiguration configuration)
        {
            _busControl = busControl;
            _logger = logger;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Hosted Service running.");
            var source = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            await _busControl.StopAsync(source.Token);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Timed Hosted Service is stopping.");
            await _busControl.StopAsync();
        }
    }
}

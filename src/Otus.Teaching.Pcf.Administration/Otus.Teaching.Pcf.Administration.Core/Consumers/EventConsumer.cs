﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Consumers
{
    public class EventConsumer : IConsumer<MessageDto>
    {
        readonly ILogger<EventConsumer> _logger;
        private readonly IRepository<Employee> _employeeRepository;

        public EventConsumer(ILogger<EventConsumer> logger, IRepository<Employee> employeeRepository)
        {
            _logger = logger;
            _employeeRepository = employeeRepository;
        }

        public async Task Consume(ConsumeContext<MessageDto> context)
        {
            if(context.Message.Content != null)
            {
                var id = JsonSerializer.Deserialize<Guid>(context.Message.Content);

                var employee = await _employeeRepository.GetByIdAsync(id);

                if(employee != null)
                {
                    employee.AppliedPromocodesCount++;
                    await _employeeRepository.UpdateAsync(employee);
                }
                Console.WriteLine($"Notification event consumed. Message: {id}");
            }
        }
    }
}

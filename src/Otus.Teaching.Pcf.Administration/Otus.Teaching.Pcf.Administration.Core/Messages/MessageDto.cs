﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Messages
{
    public class MessageDto
    {
        public string Content { get; set; }
    }
}

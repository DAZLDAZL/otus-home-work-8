﻿using Otus.Teaching.Pcf.RabbitMqShared.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IMasstransitService<T>
    {
        Task SendMessageAsync(T message);
        Task PublishMessageAsync(T message);
    }
}
